# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.2"></a>
## [0.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.1.1...pubsweet-component-xpub-dashboard@0.1.2) (2018-03-30)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.1.1"></a>
## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.1.0...pubsweet-component-xpub-dashboard@0.1.1) (2018-03-28)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.1.0"></a>
# [0.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.0.5...pubsweet-component-xpub-dashboard@0.1.0) (2018-03-27)


### Features

* **styleguide:** page per section ([0bf0836](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0bf0836))




<a name="0.0.5"></a>
## [0.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.0.4...pubsweet-component-xpub-dashboard@0.0.5) (2018-03-19)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.0.4"></a>
## [0.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-xpub-dashboard@0.0.3...pubsweet-component-xpub-dashboard@0.0.4) (2018-03-15)




**Note:** Version bump only for package pubsweet-component-xpub-dashboard

<a name="0.0.3"></a>

## 0.0.3 (2018-03-09)

**Note:** Version bump only for package pubsweet-component-xpub-dashboard
