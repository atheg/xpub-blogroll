## pubsweet-component-xpub-dashboard

A PubSweet component that provides the xpub dashboard, listing submissions that are available for the current user to take action on.
