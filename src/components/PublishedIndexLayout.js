import React from 'react'
import { Page, Section, Heading } from './molecules/Page'
import withVersion from './withVersion'
import OwnerItem from './sections/OwnerItem'

const OwnerItemWithVersion = withVersion(OwnerItem)

const PublishedIndexLayout = ({
  collections
}) => {
  return (
    <Page>
      {!collections.length && (
          <Section>
            No articles to display.
          </Section>
        )
      }

      {!!collections.length && (
        <Section>
          <Heading>Articles</Heading>
          {collections.map(project => (
            <OwnerItemWithVersion
              key={project.id}
              project={project}
            />
          ))}
        </Section>
      )}
    </Page>
  )
}

export default PublishedIndexLayout
