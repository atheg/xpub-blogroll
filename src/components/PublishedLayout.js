import React from 'react'
import SimpleEditor from 'wax-editor-react'

const PublishedLayout = ({
  currentVersion,
  handleSubmit,
  project,
  uploadFile,
  valid,
  versions,
}) => {

  return (
    <SimpleEditor
      content={currentVersion.source}
      editing="selection"
      key={currentVersion.id}
      layout="bare"
      readOnly
    />
  )
}

export default PublishedLayout
