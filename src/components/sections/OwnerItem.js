import React from 'react'

import { Button } from '@pubsweet/ui'
import { Item, Header, Body, Divider } from '../molecules/Item'

import ProjectLink from '../ProjectLink'
import VersionTitle from './VersionTitle'

const OwnerItem = ({ project, version, deleteProject }) => (
  <Item>
    <ProjectLink page="published" project={project} version={version} >
      <VersionTitle version={version} />
    </ProjectLink>
  </Item>
)

export default OwnerItem
