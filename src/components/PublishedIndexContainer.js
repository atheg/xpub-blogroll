import { compose } from 'recompose'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { actions } from 'pubsweet-client'
import { ConnectPage } from 'xpub-connect'
import PublishedIndexLayout from './PublishedIndexLayout'

export default compose(
  ConnectPage(() => [
    actions.getCollections(),
  ]),
  connect(
    state => { // maps state to props
      const { collections } = state
      return collections
    },
    null, // map dispatch to props
  ),
  withRouter,
)(PublishedIndexLayout)
